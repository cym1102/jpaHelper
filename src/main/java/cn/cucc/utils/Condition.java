package cn.cucc.utils;

public class Condition {
	public Condition(String column, String operation, Object value) {
		this.column = column;
		this.operation = operation;
		this.value = value;
	}

	public Condition(String condition) {
		this.condition = condition;
	}

	String column;
	String operation;
	Object value;
	String condition;

	
	
	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
