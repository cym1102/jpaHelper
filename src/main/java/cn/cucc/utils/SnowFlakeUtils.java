package cn.cucc.utils;

import cn.hutool.core.util.IdUtil;

public class SnowFlakeUtils {

	public static Long nextId() {
		return IdUtil.getSnowflake(0, 0).nextId();
	}

}